package 
{
	import flash.display.MovieClip;

	public class Bullet
	{
		public var model:MovieClip;
		public var damage:int;
		
		public var isDestroyed:Boolean = false;
		public var type:String;
		public var direction:int;
		public const speed:int = 40;
		
		public function Spawn(damage:int, type:String, direction:int, posX:int, posY:int):void
		{
			this.type = type;
			this.direction = direction;
			this.damage = damage;
			
			if (type == "Enemy")
			{
				model = new MCbulletEnemy;
				
			}else if(type == "Hero")
				model = new MCbulletHero;
			
			Main.layer2.addChild(model);
			
			model.x = posX;
			model.y = posY;
			model.mc_tigger.visible = false;
		}
		
		public function evUpdate():void
		{
			if (model != null && direction > 0)
			{
				model.y += speed;
				model.rotation = 5;
			}else if(model != null && direction < 0){
				model.y -= speed;
				model.rotation = 5;
			}
				
			
			evRemove();
		}
		
		public function evRemove():void
		{
			if (model != null)
			{
				if (model.y > Main.mainStage.stageHeight+model.height || model.y < -model.height || isDestroyed)
				{
					Remove();
				}
			}
		}
		
		public function Remove():void
		{
			Main.layer2.removeChild(model);
			model = null;
			isDestroyed = true;
		}
	}
}