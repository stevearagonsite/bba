package
{
	import flash.display.BlendMode;
	import flash.events.MouseEvent;


	public class Hero
	{
		public var model:MChero;
		public var shadow:MCshadowHero;
		public var bullet:Bullet;
		
		private const _animationIdle:String = "Idle";
		private const _shadowPos:int = 100;
		private const _rotationModel:int = 10;
		private const _daleyModel:int = 15;
		
		public var isDestroyed:Boolean = false;
		public var isMove:Boolean = false;
		
		public function Spawn(posX:int, posY:int):void
		{
			model = new MChero;
			Main.layer3.addChild(model);
			shadow = new MCshadowHero;
			Main.layer2.addChild(shadow);
			
			model.gotoAndPlay(_animationIdle);
			
			model.x = posX;
			model.y = posY;
			model.mc_tigger.visible = false;
			
			shadow.x = posX+_shadowPos;
			shadow.y = posY+_shadowPos;
			shadow.alpha = 0.7;
			
			shadow.blendMode = BlendMode.MULTIPLY;
			
			Main.mainStage.addEventListener(MouseEvent.MOUSE_DOWN, evMove);
			Main.mainStage.addEventListener(MouseEvent.MOUSE_UP, evStopMove);
			Main.mainStage.addEventListener(MouseEvent.CLICK, evShoot);
		}
		
		protected function evShoot(event:MouseEvent):void
		{
			if (model != null)
			{
				if (model.y > Main.mainStage.mouseY)
				{
					bullet = new Bullet;
					bullet.Spawn(20,"Hero",-1,model.x,model.y);
					Main.myBullets.push(bullet);
				}else
				{
					bullet = new Bullet;
					bullet.Spawn(10,"Hero",1,model.x,model.y);
					Main.myBullets.push(bullet);
				}
				Main.audioShootHero.play();
				Main.audioShootHero.pan = model.x * 2 / Main.mainStage.stageWidth - 1;
			}
		}
		
		protected function evStopMove(event:MouseEvent):void
		{
			isMove = false;
		}
		
		protected function evMove(event:MouseEvent):void
		{
			isMove = true;
		}
		
		public function evUpdate():void
		{
			if (model != null)
			{
				Main.gobalPosX = model.x;
				Main.gobalPosY = Main.mainStage.stageHeight-model.y;
				
				if (isMove)
				{
					model.rotation += _rotationModel;
					if (model.x >= 0 && model.x <= Main.mainStage.stageWidth)
					{
						model.x += (Main.mainStage.mouseX-model.x)/_daleyModel;
						shadow.x += (Main.mainStage.mouseX-model.x)/_daleyModel;
					}
					if (model.y >= 0 && model.y <= Main.mainStage.stageHeight)
					{
						model.y += (Main.mainStage.mouseY-model.y)/_daleyModel;
						shadow.y += (Main.mainStage.mouseY-model.y)/_daleyModel;
					}
					
				}
			}
		}
		
		public function evPause():void
		{
			if (!isDestroyed && model != null)
			{
				var i:int;
				i = model.currentFrame;
				model.gotoAndStop(i);
			}
		}
		
		public function evRemovePause():void{
			
			if (!isDestroyed && model != null){
				var i:int;
				i = model.currentFrame;
				model.gotoAndPlay(i);
			}
		}
		
		public function evDestroyed():void{
			
			if (model != null && shadow != null){
				Main.mainStage.removeEventListener(MouseEvent.MOUSE_DOWN, evMove);
				Main.mainStage.removeEventListener(MouseEvent.MOUSE_UP, evStopMove);
				Main.mainStage.removeEventListener(MouseEvent.CLICK, evShoot);

				Main.layer3.removeChild(model);
				Main.layer2.removeChild(shadow);

				model = null;
				shadow = null;
				
				isDestroyed = true;
			}
		}
	}
}