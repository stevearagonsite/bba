package Enemies
{

	public class Basic extends Enemy
	{
		public function Basic(life:int)
		{
			this.life = life;
			
			model = new MCenemy;
			Main.layer2.addChild(model);
			
			model.x = Main.instace.getRandom(0,Main.mainStage.stageWidth);
			var positionRandom:Number = Main.instace.getRandom(1,3);

			
			if (positionRandom < 2)
			{
				model.y = Main.mainStage.stageHeight+model.height;
				isComing = "TOP";
				model.scaleY = -1;
			}else
			{
				model.y = -model.height;
				isComing = "DOWN";
			}
			model.mc_tigger.visible = false;
		}

	}
}