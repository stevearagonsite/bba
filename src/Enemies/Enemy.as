package Enemies
{
	import flash.display.MovieClip;

	public class Enemy
	{
		public var model:MovieClip;
		public var bullet:Bullet;
		public var life:int = 100;
		
		public var damageBullet:int = 3;
		public var velocity:Number = 5;

		public var isDestroyed:Boolean = false;
		public var isComing:String;
		
		protected var timeToShoot:int = 1000;
		protected var currentTimeToShoot:int = timeToShoot;
		
		internal var explotionScale:Number = 1;
		internal var scoreAdd:uint = 4;
		
		public function Enemy(){
			trace("Ready enemy...");
		}
		
		public function evUpdate():void{
			evMove();
			evTimeToShoot();
			evDestroyed();
		}
		
		//this function create bullets each 5 seconds
		private function evTimeToShoot():void{
			
			if (model != null){
				currentTimeToShoot -= 1000 / Main.mainStage.frameRate;
				if (currentTimeToShoot <= 0){
					Shoot();
					currentTimeToShoot = timeToShoot;
				}
			}
		}
		
		public function Damage(i:int):void{
			
			var x:Number = Main.instace.getRandom(0,2);
			if (x > 1){
				Main.audioDamage1.play();
				Main.audioDamage1.pan = model.x * 2 / Main.mainStage.stageWidth - 1;
			}else{
				Main.audioDamage2.play();
				Main.audioDamage2.pan = model.x * 2 / Main.mainStage.stageWidth - 1;
			}
			
			life -= i;
		}
		
		public function Shoot():void{
			
			Main.audioShootEnemies.play();
			Main.audioShootEnemies.pan = model.x * 2 / Main.mainStage.stageWidth - 1;
			
			if (isComing == "TOP"){
				bullet = new Bullet;
				bullet.Spawn(damageBullet,"Enemy",-1,model.x,model.y);
				Main.myBullets.push(bullet);
			}else if (isComing == "DOWN"){
				bullet = new Bullet;
				bullet.Spawn(damageBullet,"Enemy",1,model.x,model.y);
				Main.myBullets.push(bullet);
			}
		}
		
		public function evMove():void{
			
			if (model != null && isComing == "TOP"){
				model.y -= velocity;
			}else if(model != null && isComing == "DOWN")
				model.y += velocity;
		}
		
		public function evDestroyed():void{
			
			if (!isDestroyed && model != null){
				if (isComing == "TOP"){
					if (model.y <= -model.height)EvRemove();
				}else if(isComing == "DOWN"){
					if (model.y >= Main.mainStage.stageHeight + model.height )EvRemove();
				}
				if (life <= 0){
					Main.hud.addScore(scoreAdd);
					Main.audioExplotion.play();
					Main.audioExplotion.pan = model.x * 2 / Main.mainStage.stageWidth - 1;
					Main.instace.evExplotionDestroyed(model.x,model.y,explotionScale);
					EvRemove();
				}
			}
		}
		
		public function EvRemove():void{
			Main.layer2.removeChild(model);
			model = null;
			isDestroyed = true;
		}
	}
}