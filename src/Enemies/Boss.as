package Enemies
{

	public class Boss extends Enemy
	{
		private var directionX:int = 1;
		
		public function Boss(life:int)
		{
			this.life = life;
			
			explotionScale = 2;
			damageBullet = 8;
			scoreAdd = 10;
			
			model = new MCboss;
			Main.layer2.addChild(model);
			timeToShoot = 1000;
			
			model.x = Main.instace.getRandom(0,Main.mainStage.stageWidth);
			var positionRandom:Number = Main.instace.getRandom(1,3);
			
			if (positionRandom < 2)
			{
				model.y = Main.mainStage.stageHeight+model.height;
				isComing = "TOP";
				model.scaleY = -1;
			}else
			{
				model.y = -model.height;
				isComing = "DOWN";
			}
			model.mc_tigger.visible = false;
		}
		
		override public function evMove():void
		{
			if (model != null && isComing == "TOP")
			{
				MoveX();
				if (model.y > Main.mainStage.stageHeight-Main.mainStage.stageHeight/9)
				{
					model.y -= velocity;
				}
			}else if (model != null && isComing == "DOWN")	
			{
				MoveX();
				if (model.y < Main.mainStage.stageHeight/9)
				{
					model.y += velocity;
				}
			}
		}
		
		override public function Shoot():void
		{
			if (isComing == "TOP")
			{
				bullet = new Bullet;
				bullet.Spawn(damageBullet,"Enemy",-1,model.x-150,model.y);
				Main.myBullets.push(bullet);
				
				bullet = new Bullet;
				bullet.Spawn(damageBullet,"Enemy",-1,model.x+150,model.y);
				Main.myBullets.push(bullet);
				
				bullet = new Bullet;
				bullet.Spawn(damageBullet,"Enemy",-1,model.x+50,model.y);
				Main.myBullets.push(bullet);
				
				bullet = new Bullet;
				bullet.Spawn(damageBullet,"Enemy",-1,model.x-50,model.y);
				Main.myBullets.push(bullet);
				
			}else if (isComing == "DOWN")
			{
				bullet = new Bullet;
				bullet.Spawn(damageBullet,"Enemy",1,model.x-150,model.y);
				Main.myBullets.push(bullet);
				
				bullet = new Bullet;
				bullet.Spawn(damageBullet,"Enemy",1,model.x+150,model.y);
				Main.myBullets.push(bullet);
				
				bullet = new Bullet;
				bullet.Spawn(damageBullet,"Enemy",1,model.x+50,model.y);
				Main.myBullets.push(bullet);
				
				bullet = new Bullet;
				bullet.Spawn(damageBullet,"Enemy",1,model.x-50,model.y);
				Main.myBullets.push(bullet);
			}
		}
		
		private function MoveX():void
		{
			if (directionX > 0)
			{
				model.x += velocity;
				if (model.x > Main.mainStage.stageWidth || model.x < 0)
					directionX *= -1;
			}else
			{
				model.x -= velocity;
				if (model.x > Main.mainStage.stageWidth || model.x < 0)
					directionX *= -1;
			}
		}
		
		override public function evDestroyed():void{
			
			if (!isDestroyed && model != null){
				
				if (isComing == "TOP"){
					if (life <= 0 || model.y <= -model.height){
						Main.hud.life.alpha = 08;
						Main.audioNextLevel.play();
						Main.instace.evExplotionDestroyed(model.x,model.y,explotionScale);
						Main.hud.addScore(scoreAdd);
						Main.hud.level.alpha = 0.8;
						Main.layer2.removeChild(model);
						model = null;
						isDestroyed = true;
					
					}
				}else if(isComing == "DOWN"){
					
					if (life <= 0 || model.y >= Main.mainStage.stageHeight + model.height ){
						Main.hud.life.alpha = 08;
						Main.audioNextLevel.play();
						Main.instace.evExplotionDestroyed(model.x,model.y,explotionScale);
						Main.hud.addScore(scoreAdd);
						Main.hud.level.alpha = 0.8;
						Main.layer2.removeChild(model);
						model = null;
						isDestroyed = true;
					}
				}
			}
		}
		
	}
}