package Ambient
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	
	public class Camera2D
	{
		public var view:Sprite;
		private var _targetZoom:Number;
		
		public var delayZoom:Number = 25;
		
		public function Camera2D()
		{
			view = new Sprite();
		}
		
		public function on():void
		{
			Main.mainStage.addChild(view);
		}
		
		public function off():void
		{
			Main.mainStage.removeChild(view);
		}
		
		public function set smoothZoom(value:Number):void
		{
			_targetZoom = value;
			Main.mainStage.addEventListener(Event.ENTER_FRAME, evUpdateZoom);
		}
		
		protected function evUpdateZoom(event:Event):void
		{
			zoom += (_targetZoom - zoom) / delayZoom;
			
			if(Math.abs(_targetZoom - zoom) < 0.001)
			{
				Main.mainStage.removeEventListener(Event.ENTER_FRAME, evUpdateZoom);
				zoom = _targetZoom;
			}
		}
		
		public function set zoom(value:Number):void
		{
			//trace("SET");
			if(value > 0)
			{
				view.scaleX = view.scaleY = value;
			}else
			{
				throw new Error("El zoom no puede ser 0 o menos.");
			}
		}
		
		public function get zoom():Number
		{
			//trace("GET");
			return view.scaleX;
		}
		
		public function set x(value:Number):void
		{
			view.x = -value;
		}
		
		public function get y():Number
		{
			return -view.y;
		}
		
		public function set y(value:Number):void
		{
			view.y = -value;
		}
		
		public function get x():Number
		{
			return -view.x;
		}
		
		public function lookAt(target:MovieClip):void
		{
			/*x = (target.x * zoom - Main.mainStage.stageWidth / 2);
			y = (target.y * zoom - Main.mainStage.stageHeight / 2);*/
			
			var localPosition:Point = new Point(target.x, target.y);
			var globalPosition:Point = target.parent.localToGlobal(localPosition);
			var viewPosition:Point = view.globalToLocal(globalPosition);
			
			x = viewPosition.x * zoom - Main.mainStage.stageWidth / 2;
			y = viewPosition.y * zoom - Main.mainStage.stageHeight / 2;
			
		}
		
		public function smoothLookAt(target:MovieClip):void
		{
			var localPosition:Point = new Point(target.x, target.y);
			var globalPosition:Point = target.parent.localToGlobal(localPosition);
			var viewPosition:Point = view.globalToLocal(globalPosition);
			
			x += (viewPosition.x * zoom - Main.mainStage.stageWidth / 2 - x) / 25;
			y += (viewPosition.y * zoom - Main.mainStage.stageHeight / 2 - y) / 25;
		}
		
		public function addToView(obj:MovieClip):void
		{
			view.addChild(obj);
		}
		
		public function removeToView(obj:MovieClip):void
		{
			view.removeChild(obj);
		}
	}
}