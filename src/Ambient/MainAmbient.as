package Ambient{

	public class MainAmbient{
		private var _model:MCbackgroundGame = new MCbackgroundGame;//movieclip have background for the game
		private var _model2:MCbackgroundGame2 = new MCbackgroundGame2;//movieclip have background num 2 for the game
		public static var velocity:Number = 0.05;//here i have the velocity for the backgrounds and the mountains
		
		private var _timeToSpawnMountain:int = 5000;//this is the initials parameters for spawn mountains
		private var _currentTimeToSpawnMountain:int = _timeToSpawnMountain;//i can to use for the timer for the spawn mountains
		
		private var _timeToChangeMove:int = 2000;//this is the initials parameters for move fog
		private var _currentTimeToChangeMove:int = _timeToChangeMove;//i can control velocity move of the fog
		
		private var _myMountains:Array = new Array;//here have all the mountains (the class Mountains)
		private var _mountain:Mountains;//this is the class mountain
		
		private var _fog:MCfog = new MCfog;//this is the fog in the ambient
		private var _fog2:MCfog = new MCfog;//this is the fog 2 in the ambient
		private var _velocityFog:Number = 2;//this value is the velocity the fog
		private var _directionFog:int = 1;//i can control move horizontal the fog
		private var _alphaValueFog:int = 1;//i can control alpha
		
		public function spawn():void{//this is the initials parameters for the background in the game
		
			Main.layer1.addChild(_model);
			Main.layer1.addChild(_model2);
			
			Main.layer4.addChild(_fog);
			Main.layer4.addChild(_fog2);
			
			_fog.alpha = _fog2.alpha = 0.7;
			
			_fog2.y = -_fog2.height;
			_model2.y = -_model2.height;
		}
		
		public function evUpdate():void{//here i can update the background and speed in this
		
			evUpdateBackground();//here is the backgrounds
			evTimerMountains();//here create mountain
			evUpdateMountains();//here is the mountains
			evTimerMoveFog();//the fog move value control here
			evUpdateFog();//control the fog here
		}
		
		private function evUpdateFog():void{//here ai fog
		
			if (_fog != null){
				//--------------control horizontal
				_fog.x += _directionFog*_velocityFog;
				_fog2.x += _directionFog*_velocityFog;
				
				if (_fog.x >= 0)_directionFog = -1;
				if(_fog.x <= Main.mainStage.stageWidth-_fog.width)_directionFog = 1;
				//--------------control vertical
				_fog.y += Main.gobalPosY*velocity;
				_fog2.y += Main.gobalPosY*velocity;
				
				if (_fog.y >= Main.mainStage.stageHeight)_fog.y = _fog2.y-_fog.height;
				if (_fog2.y >= Main.mainStage.stageHeight) _fog2.y = _fog.y-_fog2.height;
				//--------------control alpha
				_fog.alpha += (_alphaValueFog*0.001)/2;
				_fog2.alpha += (_alphaValueFog*0.001)/2;
				
				if (_fog.alpha <= 0.1)_alphaValueFog = 1;
				if (_fog.alpha >= 0.7)_alphaValueFog = -1;
			}
		}
		
		private function evUpdateMountains():void{//here execute update mountain by mountain in the for
		
			for (var i:int = _myMountains.length-1; i >= 0; i--){
				
				if (_myMountains[i].isDestroyed){
					_myMountains[i] = null;
					_myMountains.splice(i,1);
				}else _myMountains[i].evUpdate();
			}
			
		}
		
		private function evTimerMountains():void{//this is the control to spawn mountain
		
			_currentTimeToSpawnMountain -= 1000/Main.mainStage.frameRate;
			if (_currentTimeToSpawnMountain <= 0){
				
				_timeToSpawnMountain = Main.instace.getRandom(2000,6000);
				_currentTimeToSpawnMountain = _timeToSpawnMountain;
				evSpawnMountain();
			}
		}
		
		private function evTimerMoveFog():void{//here create control move for fog
		
			_currentTimeToChangeMove -= 1000/Main.mainStage.frameRate;
			if (_currentTimeToChangeMove <= 0){
				_velocityFog = Main.instace.getRandom(1,5);
				_timeToChangeMove = Main.instace.getRandom(2000,6000);
				_currentTimeToChangeMove = _timeToChangeMove;
			}
		}
		
		
		private function evSpawnMountain():void{//here create the mountain and push in the myMountains
		
			_mountain = new Mountains;
			_mountain.spawn();
			
			_myMountains.push(_mountain);
		}
		
		private function evUpdateBackground():void{//here i can control for the background update
		
			if (_model != null || _model2 != null){
				
				_model.y += Main.gobalPosY*velocity;
				_model2.y += Main.gobalPosY*velocity;
				
				if (_model.y >= Main.mainStage.stageHeight) _model.y = _model2.y-_model.height;
				if (_model2.y >= Main.mainStage.stageHeight)_model2.y = _model.y-_model2.height;
			}
		}
		
		public function evRemove():void{
			
			if (_model != null && _model2 != null){
				Main.layer1.removeChild(_model);
				Main.layer1.removeChild(_model2);
				
				_model = null;
				_model2 = null;
			}
			
			if (_fog != null && _fog2 != null){
			
				Main.layer4.removeChild(_fog);
				Main.layer4.removeChild(_fog2);
				
				_fog = null;
				_fog2 = null;
			}
			
			if (_myMountains.length-1 > 0){
			
				for (var i:int = _myMountains.length-1; i >= 0; i--) {
					if (_myMountains[i].isDestroyed){
						_myMountains[i] = null;
						_myMountains.splice(i,1);
					}else _myMountains[i].evDestroyed();
				}
			}else _myMountains = null;
		}
			
	}
		
}