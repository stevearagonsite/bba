package Ambient
{

	public class Mountains
	{
		public var mountain:MCmountain;//movieclip have a mountain
		public var mountain2:MCmountain2;//movieclip have a mountain
		public var mountain3:MCmountain3;//movieclip have a mountain
		public var mountain4:MCmountain4;//movieclip have a mountain
		
		public var typeMountain:int;//here i can selection a mountain
		public var velocity:Number = MainAmbient.velocity;//velocity gerenal system (from the hero)
		
		public var isDestroyed:Boolean = false;//i can to know if this class have a movieclip
		
		public function spawn():void//here create the movieclip by random in the main
		{
			typeMountain = Main.instace.getRandom(1,5);
			switch(typeMountain)
			{
				case 1:
				{
					mountain = new MCmountain;
					Main.layer1.addChild(mountain);
					mountain.alpha = 0.3;
					
					mountain.scaleX = Main.instace.getRandom(0.5,2);
					mountain.scaleY = Main.instace.getRandom(0.5,2);
					mountain.rotation = Main.instace.getRandom(0,360);
					mountain.x = Main.instace.getRandom(0,Main.mainStage.stageWidth);
					mountain.y = -mountain.height;
					
					break;
				}
					
				case 2:
				{
					mountain2 = new MCmountain2;
					Main.layer1.addChild(mountain2);
					mountain2.alpha = 0.3;
					
					mountain2.scaleX = Main.instace.getRandom(0.5,2);
					mountain2.scaleY = Main.instace.getRandom(0.5,2);
					mountain2.rotation = Main.instace.getRandom(0,360);
					mountain2.x = Main.instace.getRandom(0,Main.mainStage.stageWidth);
					mountain2.y = -mountain2.height;
					
					break;
				}
					
				case 3:
				{
					mountain3 = new MCmountain3;
					Main.layer1.addChild(mountain3);
					mountain3.alpha = 0.3;
					
					mountain3.scaleX = Main.instace.getRandom(0.5,2);
					mountain3.scaleY = Main.instace.getRandom(0.5,2);
					mountain3.rotation = Main.instace.getRandom(0,360);
					mountain3.x = Main.instace.getRandom(0,Main.mainStage.stageWidth);
					mountain3.y = -mountain3.height;
					
					break;
				}
					
				case 4:
				{
					mountain4 = new MCmountain4;
					Main.layer1.addChild(mountain4);
					mountain4.alpha = 0.3;
					
					mountain4.scaleX = Main.instace.getRandom(0.5,2);
					mountain4.scaleY = Main.instace.getRandom(0.5,2);
					mountain4.rotation = Main.instace.getRandom(0,360);
					mountain4.x = Main.instace.getRandom(0,Main.mainStage.stageWidth);
					mountain4.y = -mountain4.height;
					
					break;
				}
			}
		}
		
		public function evUpdate():void//this is the update for class (mountain by mountain)
		{
			switch(typeMountain)
			{
				case 1:
				{
					if (mountain.y <= Main.mainStage.stageHeight+Main.mainStage.stageHeight/2)
					{
						mountain.y += Main.gobalPosY*velocity;
					}else
					{
						Main.layer1.removeChild(mountain);
						mountain = null;
						
						isDestroyed = true;
					}
					break;
				}
					
				case 2:
				{
					if (mountain2.y <= Main.mainStage.stageHeight+Main.mainStage.stageHeight/2)
					{
						mountain2.y += Main.gobalPosY*velocity;
					}else
					{
						Main.layer1.removeChild(mountain2);
						mountain2 = null;
						
						isDestroyed = true;
					}
					break;
				}
					
				case 3:
				{
					if (mountain3.y <= Main.mainStage.stageHeight+Main.mainStage.stageHeight/2)
					{
						mountain3.y += Main.gobalPosY*velocity;
					}else
					{
						Main.layer1.removeChild(mountain3);
						mountain3 = null;
						
						isDestroyed = true;
					}
					break;

				}
					
				case 4:
				{
					if (mountain4.y <= Main.mainStage.stageHeight+Main.mainStage.stageHeight/2)
					{
						mountain4.y += Main.gobalPosY*velocity;
					}else
					{
						Main.layer1.removeChild(mountain4);
						mountain4 = null;
						
						isDestroyed = true;
					}
					break;
				}
			}
		}
		
		public function evDestroyed():void
		{
			switch(typeMountain)
			{
				case 1:
				{
					Main.layer1.removeChild(mountain);
					mountain = null;
					isDestroyed = true;
					break;
				}
					
				case 2:
				{
					Main.layer1.removeChild(mountain2);
					mountain2 = null;
					isDestroyed = true;
					break;
				}
					
				case 3:
				{
					Main.layer1.removeChild(mountain3);
					mountain3 = null;
					isDestroyed = true;
					break;
				}
					
				case 4:
				{
					Main.layer1.removeChild(mountain4);
					mountain4 = null;
					isDestroyed = true;
					break;
				}
			}
		}
	}
}