package Ambient
{
	import flash.events.Event;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	
	public class SoundController{
		public var snd:Sound;
		public var channel:SoundChannel;
		public var settings:SoundTransform;
		public var lastPosition:Number;
		
		public function SoundController(snd:Sound){
			this.snd = snd;
		}
		
		public function play():void{
			settings = new SoundTransform(1, 0);
			channel = snd.play(0, 0, settings);
		}
		
		public function playBackground():void{
			
			settings = new SoundTransform(1, 0);
			channel = snd.play(0, 999999, settings);
			
			if(channel != null)
			{
				channel.addEventListener(Event.SOUND_COMPLETE, evSoundComplete);
			}else
			{
				throw new Error("you dont have speakers..");
			}
		}
		
		protected function evSoundComplete(event:Event):void
		{
			playBackground();
		}
		
		public function stop():void
		{
			channel.stop();
		}
		
		public function pause():void
		{
			lastPosition = channel.position; //Guarda la posición en la que está el canal en milisegundos.
			channel.stop();
		}
		
		public function resume():void
		{
			channel = snd.play(lastPosition, 0, settings);
		}
		
		public function set volume(value:Number):void
		{
			settings.volume = value;
			channel.soundTransform = settings;
		}
		
		public function get volume():Number
		{
			return settings.volume;
		}
		
		public function set pan(value:Number):void
		{
			settings.pan = value;
			channel.soundTransform = settings;
		}
		
		public function get pan():Number
		{
			return settings.pan;
		}
		
		public function set x(value:Number):void
		{
			pan = value * 2 / Main.mainStage.stageWidth - 1;
		}
	}
}