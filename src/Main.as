package
{
	import com.greensock.plugins.MotionBlurPlugin;
	import com.greensock.plugins.TweenPlugin;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.display.StageDisplayState;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	import flash.ui.Mouse;
	
	import Ambient.Camera2D;
	import Ambient.MainAmbient;
	import Ambient.SoundController;
	
	import Enemies.Basic;
	import Enemies.Boss;
	import Enemies.Enemy;
	
	import GUI.HUD;
	import GUI.Menu;
	
	
	
	[SWF(width="1920", height="1080", frameRate="30", backgroundColor="0x000000")]
	public class Main extends Sprite{
		
		public static var mainStage:Stage;//here i can to use the stage in other class.
		public static var instace:Main;//here i can to use for run function.
		public var menu:Menu;//here have the options for the game.
		public var gameOver:MCgameOver;
		
		public var stateFullScreen:Boolean = true;//Screen State.
		public var isPlaying:Boolean = false;//Screen State.
		public var isPause:Boolean = false;//Screen State.
		
		public var cam:Camera2D;//here is the view and events FX.
		public var audio:SoundController;//here is events SFX and Sountrack.
		
		public static var level:MovieClip = new MovieClip;//here save my assets games for the camera and others FX.
		public static var layer1:Sprite = new Sprite;//here save background.
		public static var layer2:Sprite = new Sprite;//here is the enemies.
		public static var layer3:Sprite = new Sprite;//here is the Boss and Hero.
		public static var layer4:Sprite = new Sprite;//i have the HUD and FX.
		
		public var ambient:MainAmbient;
		public static var hud:HUD;
		public static var hero:Hero;
		public var enemy:Enemy;
		public var dark:MCdark;
		
		private var _timeToSpawnEnemy:int = 10000;
		private var _currentTimeToSpawnEnemy:Number = _timeToSpawnEnemy; 
		private var _pointToSpawnBoss:int = 50;
		public static var totalPoints:int = 0;
		public static var lvl:int =1;
		
		public static var myEnemies:Array = new Array();
		public static var myBullets:Array = new Array();
				
		public static var gobalPosX:int;//get position for the ambient and fx
		public static var gobalPosY:int = 500;//i can control fx for the aceleration
		
		public static var audioBackground:SoundController = new SoundController( new SNDtrack());
		public static var audioBackgroundMenu:SoundController = new SoundController( new SNDtrackMenu());
		public static var audioDamage1:SoundController = new SoundController( new SNDdamage1());
		public static var audioDamage2:SoundController = new SoundController( new SNDdamage2());
		public static var audioExplotion:SoundController = new SoundController( new SNDexplotion());
		public static var audioShootEnemies:SoundController = new SoundController( new SNDshootEnemies());
		public static var audioShootHero:SoundController = new SoundController( new SNDshootHero());
		public static var audioNextLevel:SoundController = new SoundController( new SNDnextLevel());
		
		public function Main(){
			
			TweenPlugin.activate([MotionBlurPlugin]);//crack tweenMax
			
			mainStage = stage;//here have my moviclips
			instace = this;//this execute in other class
			audioBackgroundMenu.playBackground();
			
			//this is screen parameters
			
			mainStage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;//here i can scale the elements
			mainStage.scaleMode = StageScaleMode.EXACT_FIT;//here scale the elements
			mainStage.addEventListener(KeyboardEvent.KEY_DOWN, EvKeys);
			
			level.addChild(layer1);//this element is child of level, is save background FX and the asteroids
			level.addChild(layer2);//this element is child of level, is the enemies
			level.addChild(layer3);//this element is child of level, is the hero
			level.addChild(layer4);//this element is child of level, is HUD and FX
			
			evMenu();
		}
		
		protected function EvKeys(event:KeyboardEvent):void{
			
			if (event.keyCode == Keyboard.ESCAPE && stateFullScreen){
				mainStage.nativeWindow.x = mainStage.nativeWindow.y = 0;//I can control the windows position.
				/**--------------------------problems--------------------**/
				//mainStage.nativeWindow.width = 800;//I can control size width.
				//mainStage.nativeWindow.height = 600;//I can control size height.
				/**--------------------------end problems--------------------**/
				stateFullScreen = false;//Value state screen.
			}
			
			if (event.keyCode == Keyboard.F4 && !stateFullScreen){
				mainStage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;//here i can scale the elements
				mainStage.scaleMode = StageScaleMode.SHOW_ALL;//here scale the elements
				stateFullScreen = true;//value state screen.
			}
			
			if (event.keyCode == Keyboard.F2 && isPlaying){
				if (!isPause){
					mainStage.removeEventListener(Event.ENTER_FRAME, evUpdate);
					hero.evPause();
					audioBackground.pause();
				}else{
					mainStage.addEventListener(Event.ENTER_FRAME, evUpdate);
					hero.evRemovePause();
					audioBackground.resume();
				}
				isPause = !isPause;
			}
		}
		
		public function evMenu():void{//here execute the menu class, this class execute evStartGame
		
			if (gameOver != null){
				gameOver.removeEventListener(MouseEvent.MOUSE_UP, evReset);
				
				mainStage.removeChild(gameOver);
				gameOver = null;
			}
			menu = new Menu;
			menu.spawn();//painting the menu and execute the events for the user
		}
		
		public function evStartGame():void{//here create the game and events for this
		
			ambient = new MainAmbient;//this class have wallpaper and FX velocitiy
			ambient.spawn();//create the moviclip in the mainStage
			audioBackgroundMenu.stop();
			audioBackground.playBackground();
			audioBackground.volume = 0.5;
			
			cam = new Camera2D();
			cam.on();//active the camera
			cam.addToView(level);//camera is parent the level(objets zoom)
			
			hero = new Hero;
			hero.Spawn(mainStage.stageWidth/2, mainStage.stageHeight/2);
			
			dark = new MCdark;
			layer4.addChild(dark);
			Mouse.hide();
			
			hud = new HUD;
			hud.spawn();
			
			mainStage.addEventListener(Event.ENTER_FRAME, evUpdate);
			mainStage.addEventListener(KeyboardEvent.KEY_UP, evKey);
			isPlaying = true;
		}
		
		protected function evKey(event:KeyboardEvent):void{
			if (event.keyCode == Keyboard.F3)
				evRemove();
		}		
		
		protected function evUpdate(event:Event):void{//all update is here
			
			ambient.evUpdate();//here execute update background and mountains
			hero.evUpdate();//here i can move my hero
			evDark();//events the move dark
			evTimeToSpawnEnemy();
			
			evUpdateBullets();
			evHits();
			evUpdateEnemies();
			if (hud != null)hud.evUpdate();//here i show the points, life and damage;
		}
		
		private function evHits():void{
			
			for (var i_bullets:int = myBullets.length-1; i_bullets >= 0; i_bullets--){//check bullets
			
				for (var i_enemies:int = myEnemies.length-1; i_enemies >= 0 ; i_enemies--){//check enemies
					if (!myBullets[i_bullets].isDestroyed 
						&& !myEnemies[i_enemies].isDestroyed 
						&& myBullets[i_bullets].type == "Hero" 
						&& myBullets[i_bullets].model.mc_tigger.hitTestObject(myEnemies[i_enemies].model.mc_tigger)){//to be bullets and enemies??, check collision

						!myEnemies[i_enemies].Damage(myBullets[i_bullets].damage);
						evExplotion(myBullets[i_bullets].model.x ,myBullets[i_bullets].model.y,1);
						myBullets[i_bullets].Remove();//----deleted the movieclip in the screen
						break;
					}
				}
				
				if (!myBullets[i_bullets].isDestroyed 
					&& !hero.isDestroyed && myBullets[i_bullets].type == "Enemy" &&
					myBullets[i_bullets].model.mc_tigger.hitTestObject(hero.model.mc_tigger)){
					
					myBullets[i_bullets].Remove();//----deleted the movieclip in the screen
					if (hud != null){
						hud.screenDamage.alpha = hud.life.alpha = hud.move.alpha = hud.score.alpha = 0.8;
						hud.removeLife(myBullets[i_bullets].damage);
					}
					break;
				}
			}
		}
		
		private function evUpdateBullets():void{
			for(var i:int = myBullets.length - 1; i >= 0; i--){
				if (myBullets[i] != null){
					if (myBullets[i].isDestroyed){
						myBullets[i] = null;
						myBullets.splice(i,1);
					}else myBullets[i].evUpdate();
				}
			}
		}
		
		private function evUpdateEnemies():void{
			
			for(var i:int = myEnemies.length - 1; i >= 0; i--){
				if (myEnemies[i] != null){
					if (myEnemies[i].isDestroyed){
						myEnemies[i] = null;
						myEnemies.splice(i,1);
					}else myEnemies[i].evUpdate();
				}
			}
			if (totalPoints > _pointToSpawnBoss){
				enemy = new Boss(200);
				myEnemies.push(enemy);
				_pointToSpawnBoss *= 2;
				hud.evLevel(1);
			}
			
		}
		
		public function evExplotion(posX:int, posY:int, scale:Number):void{
			//take position collision and spawn feedback, the enemy send posX and PosY
		
			var explotion:MCdamageEnemy= new MCdamageEnemy;
			layer3.addChild(explotion);
			
			explotion.x = posX;
			explotion.y = posY;
			
			explotion.scaleX = explotion.scaleY = scale;
		}
		
		public function evExplotionDestroyed(posX:int, posY:int, scale:Number):void{
			//take position collision and spawn feedback, the enemy send posX and PosY
		
			var explotion:MCbam = new MCbam;
			layer3.addChild(explotion);
			
			explotion.x = posX;
			explotion.y = posY;
			
			explotion.scaleX = explotion.scaleY = scale;
		}
		
		
		private function evTimeToSpawnEnemy():void{
			
			_currentTimeToSpawnEnemy -= 1000/mainStage.frameRate;
			if (_currentTimeToSpawnEnemy < 0){
				_currentTimeToSpawnEnemy = _timeToSpawnEnemy;
				evSpawnEnemy();
			}
		}
		
		private function evSpawnEnemy():void{
			enemy = new Basic(100);
			myEnemies.push(enemy);
		}
		
		public function evGameOver():void{
			evRemove();
			audioBackground.stop();
			audioBackgroundMenu.playBackground();
			gameOver = new MCgameOver;
			gameOver.tb_level.text = ""+lvl;
			gameOver.tb_points.text = ""+totalPoints;
			
			mainStage.addChild(gameOver);
			gameOver.addEventListener(MouseEvent.MOUSE_UP, evReset);
		}
		
		protected function evReset(event:MouseEvent):void{
			Mouse.show();
			evMenu();
		}
		
		private function evDark():void{//here execute the move dark
		
			if (dark != null){
				dark.x = mainStage.mouseX;
				dark.y = mainStage.mouseY;
			}
		}
		
		public function evPause():void{mainStage.removeEventListener(Event.ENTER_FRAME, evUpdate);}
		
		public function evRemovePause():void{mainStage.addEventListener(Event.ENTER_FRAME, evUpdate);}
		
		public function getRandom(min:int, max:int):Number{return Math.random()* (max-min)+min;}
		
		public function evRemove():void{//here deleted all objetcs and events
		
			mainStage.removeEventListener(Event.ENTER_FRAME, evUpdate);
			if (dark != null){
				layer4.removeChild(dark);
				dark = null;  
			}
			if (!hero.isDestroyed){
				hero.evDestroyed();
				hero = null;
			}
			if (ambient != null){
				ambient.evRemove();
				ambient = null;
			}
			if (cam != null){
				cam.off();
				cam = null;
			}
			if (hud != null){
				hud.evRemove();
				hud = null;
			}
			
			for(var i:int = myEnemies.length - 1; i >= 0; i--){
				
				if (myEnemies[i] != null){
					myEnemies[i].isDestroyed = true;
					myEnemies[i].EvRemove();
					myEnemies[i] = null;
					myEnemies.splice(i,1);
				}
			}
			
			for(var i_bullets:int = myBullets.length - 1; i_bullets >= 0; i_bullets--){
				
				if(myBullets[i_bullets] != null){
					
					myBullets[i_bullets].isDestroyed = true
					myBullets[i_bullets].evRemove();
					
					myBullets[i_bullets] = null;
					myBullets.splice(i_bullets,1);
				}
			}
		}
	}
}