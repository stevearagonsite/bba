package GUI
{
	import flash.events.MouseEvent;

	public class Menu
	{
		public var intro:MCbackground;//this movieclip have background intro 
		public var buttonIntro:MCbuttonTitle;//this button is an animation for the intro
		public var menu:MCbackground2;//this movieclip have background menu
		public var buttonPlay:MCbuttonPlay;//this button is an animation in the menu
		public var buttonHowToPlay:MCbuttonHowToPlay;//this button is an animation in the menu
		public var buttonCredits:MCbuttonCredits;//this button is an animation in the menu
		
		public var howToPlay:MCbackground3;//here create the screen for how to play
		public var credits:MCbackground4;//here create the screen for credits
		public var buttonArrow:MCbuttonArrow;//this button is an animation in the sub-menu
		
		public const buttonLabelOver:String = "MouseOver";
		public const buttonLabelDown:String = "MouseDown";
		
		public function spawn():void
		{
			//here create the ambient for the intro in the game
			intro = new MCbackground;
			Main.mainStage.addChild(intro);
			
			buttonIntro = new MCbuttonTitle;
			Main.mainStage.addChild(buttonIntro);
			
			buttonIntro.gotoAndStop(1);
			
			buttonIntro.x = 960;
			buttonIntro.y = 540;
			//here create the events for the intro
			buttonIntro.addEventListener(MouseEvent.MOUSE_OVER, evButtonIntroOver);
			buttonIntro.addEventListener(MouseEvent.MOUSE_OUT, evButtonIntroOut);
			buttonIntro.addEventListener(MouseEvent.MOUSE_DOWN, evButtonIntroDown);
			buttonIntro.addEventListener(MouseEvent.MOUSE_UP, evButtonIntroUp);
			Main.mainStage.addEventListener(MouseEvent.CLICK, EvSoundClick);
		}
		
		protected function EvSoundClick(event:MouseEvent):void{
			Main.audioShootHero.play();
		}
		
		protected function evButtonIntroDown(event:MouseEvent):void
		{	
			if (buttonIntro != null)
			{
				buttonIntro.gotoAndPlay(buttonLabelDown);
			}
		}
		
		protected function evButtonIntroOut(event:MouseEvent):void
		{
			if (buttonIntro != null)
			{
				buttonIntro.gotoAndStop(1);
			}
		}
		
		protected function evButtonIntroOver(event:MouseEvent):void
		{
			if (buttonIntro != null)
			{
				buttonIntro.gotoAndPlay(buttonLabelOver);
			}
		}
		
		protected function evButtonIntroUp(event:MouseEvent):void
		{
			evGoMenu();
		}
		
		public function evGoMenu():void
		{
			evRemove();
			//this is the background for the menu
			menu = new MCbackground2;
			Main.mainStage.addChild(menu);
			//this is the buttons for the menu
			buttonPlay = new MCbuttonPlay;
			Main.mainStage.addChild(buttonPlay);
			
			buttonHowToPlay = new MCbuttonHowToPlay;
			Main.mainStage.addChild(buttonHowToPlay);
			
			buttonCredits = new MCbuttonCredits;
			Main.mainStage.addChild(buttonCredits);
			
			buttonPlay.x = buttonCredits.x = buttonHowToPlay.x = 960;
			buttonPlay.y = 400;
			buttonHowToPlay.y = 550;
			buttonCredits.y = 700;
			
			buttonPlay.gotoAndStop(1);
			buttonHowToPlay.gotoAndStop(1);
			buttonCredits.gotoAndStop(1);
			//here create the events for buttons in the menu
			buttonPlay.addEventListener(MouseEvent.MOUSE_OVER, evButtonPlayOver);
			buttonPlay.addEventListener(MouseEvent.MOUSE_OUT, evButtonPlayOut);
			buttonPlay.addEventListener(MouseEvent.MOUSE_DOWN, evButtonPlayDown);
			buttonPlay.addEventListener(MouseEvent.MOUSE_UP, evButtonPlayUp);
			
			buttonHowToPlay.addEventListener(MouseEvent.MOUSE_OVER, evButtonHowToPlayOver);
			buttonHowToPlay.addEventListener(MouseEvent.MOUSE_OUT, evButtonHowToPlayOut);
			buttonHowToPlay.addEventListener(MouseEvent.MOUSE_DOWN, evButtonHowToPlayDown);
			buttonHowToPlay.addEventListener(MouseEvent.MOUSE_UP, evButtonHowToPlayUp);
			
			buttonCredits.addEventListener(MouseEvent.MOUSE_OVER, evButtonCreditsOver);
			buttonCredits.addEventListener(MouseEvent.MOUSE_OUT, evButtonCreditsOut);
			buttonCredits.addEventListener(MouseEvent.MOUSE_DOWN, evButtonCreditsDown);
			buttonCredits.addEventListener(MouseEvent.MOUSE_UP, evButtonCreditsUp);
			
		}
//----------------------------------------------------------------------------------------------loading.... (mouseDown)
		protected function evButtonCreditsUp(event:MouseEvent):void
		{
			evCredits();
		}
		protected function evButtonCreditsDown(event:MouseEvent):void
		{
			if (buttonCredits != null)
			{
				buttonCredits.gotoAndStop(buttonLabelDown);
			}
		}
		
		protected function evButtonCreditsOut(event:MouseEvent):void
		{
			if (buttonCredits != null)
			{
				buttonCredits.gotoAndStop(1);
			}
		}
		
		protected function evButtonCreditsOver(event:MouseEvent):void
		{
			if (buttonCredits != null)
			{
				buttonCredits.gotoAndPlay(buttonLabelOver);
			}
		}
		
		protected function evButtonHowToPlayUp(event:MouseEvent):void
		{
			evHowToPlay();
		}
		protected function evButtonHowToPlayDown(event:MouseEvent):void
		{
			if (buttonHowToPlay != null)
			{
				buttonHowToPlay.gotoAndStop(buttonLabelDown);
			}
		}
		
		protected function evButtonHowToPlayOut(event:MouseEvent):void
		{
			if (buttonHowToPlay != null)
			{
				buttonHowToPlay.gotoAndStop(1);
			}
		}
		
		protected function evButtonHowToPlayOver(event:MouseEvent):void
		{
			if (buttonHowToPlay != null)
			{
				buttonHowToPlay.gotoAndPlay(buttonLabelOver);
			}
		}
		
		protected function evButtonPlayUp(event:MouseEvent):void
		{
			evPlay();
		}
		protected function evButtonPlayDown(event:MouseEvent):void
		{
			if (buttonPlay != null)
			{
				buttonPlay.gotoAndStop(buttonLabelDown);
			}
		}
		
		protected function evButtonPlayOut(event:MouseEvent):void
		{
			if (buttonPlay != null)
			{
				buttonPlay.gotoAndStop(1);
			}
		}
		
		protected function evButtonPlayOver(event:MouseEvent):void
		{
			if (buttonPlay != null)
			{
				buttonPlay.gotoAndPlay(buttonLabelOver);
			}
		}
//-------------------------------------------------------------------------------------------				
		private function evCredits():void//this function create screen for credits 
		{
			evRemove();//remove all menu
			//here create the content and background
			credits = new MCbackground4;
			Main.mainStage.addChild(credits);
			//here create the buttons for this screen
			evSubMenu();
		}
		private function evHowToPlay():void//this function create the screen for explain how to play
		{
			evRemove();//remove all menu
			//here create the content and background
			howToPlay = new MCbackground3;
			Main.mainStage.addChild(howToPlay);
			//here create the buttons for this screen
			evSubMenu();
		}
		
		private function evPlay():void//here go to Main and execute the function star game (evStartGame)
		{
			evRemove();//here remove all in this class
			Main.mainStage.removeEventListener(MouseEvent.CLICK, EvSoundClick);
			Main.instace.evStartGame();//here execute the game
		}
		
		
		private function evSubMenu():void//here create the menu for credits and how to play
		{
			buttonArrow = new MCbuttonArrow;
			Main.mainStage.addChild(buttonArrow);
			
			buttonArrow.scaleX = buttonArrow.scaleY = -0.5;
			buttonArrow.x = 100;
			buttonArrow.y = 900;
			
			buttonArrow.gotoAndStop(1);
			
			buttonArrow.addEventListener(MouseEvent.MOUSE_OVER, evButtonArrowOver);
			buttonArrow.addEventListener(MouseEvent.MOUSE_OUT, evButtonArrowOut);
			buttonArrow.addEventListener(MouseEvent.MOUSE_DOWN, evButtonArrowDown);
			buttonArrow.addEventListener(MouseEvent.MOUSE_UP, evButtonArrowUp);
			
		}		
//------------------------------------------------------sub menu mouse events
		protected function evButtonArrowUp(event:MouseEvent):void
		{
			evGoMenu();
		}
		
		protected function evButtonArrowDown(event:MouseEvent):void
		{
			buttonArrow.gotoAndStop(buttonLabelDown);
		}
		
		protected function evButtonArrowOut(event:MouseEvent):void
		{
			buttonArrow.gotoAndStop(1);
		}
		
		protected function evButtonArrowOver(event:MouseEvent):void
		{
			buttonArrow.gotoAndPlay(buttonLabelOver);
		}
//------------------------------------------------------------------------------
		private function evRemove():void
		{
			if (buttonIntro != null && intro != null)
			{
				//remove events
				buttonIntro.removeEventListener(MouseEvent.MOUSE_OVER, evButtonIntroOver);
				buttonIntro.removeEventListener(MouseEvent.MOUSE_OUT, evButtonIntroOut);
				buttonIntro.removeEventListener(MouseEvent.MOUSE_DOWN, evButtonIntroDown);
				buttonIntro.removeEventListener(MouseEvent.MOUSE_DOWN, evButtonIntroUp);
				//remove object
				Main.mainStage.removeChild(buttonIntro);
				Main.mainStage.removeChild(intro);
				
				intro = null;
				buttonIntro = null;
			}
			if (menu != null)
			{
				//remove events
				buttonPlay.removeEventListener(MouseEvent.MOUSE_OVER, evButtonPlayOver);
				buttonPlay.removeEventListener(MouseEvent.MOUSE_OUT, evButtonPlayOut);
				buttonPlay.removeEventListener(MouseEvent.MOUSE_DOWN, evButtonPlayDown);
				buttonPlay.removeEventListener(MouseEvent.MOUSE_UP, evButtonPlayUp);
				
				buttonHowToPlay.removeEventListener(MouseEvent.MOUSE_OVER, evButtonHowToPlayOver);
				buttonHowToPlay.removeEventListener(MouseEvent.MOUSE_OUT, evButtonHowToPlayOut);
				buttonHowToPlay.removeEventListener(MouseEvent.MOUSE_DOWN, evButtonHowToPlayDown);
				buttonHowToPlay.removeEventListener(MouseEvent.MOUSE_UP, evButtonHowToPlayUp);
				
				buttonCredits.removeEventListener(MouseEvent.MOUSE_OVER, evButtonCreditsOver);
				buttonCredits.removeEventListener(MouseEvent.MOUSE_OUT, evButtonCreditsOut);
				buttonCredits.removeEventListener(MouseEvent.MOUSE_DOWN, evButtonCreditsDown);
				buttonCredits.removeEventListener(MouseEvent.MOUSE_UP, evButtonCreditsUp);
				//remove objects 
				Main.mainStage.removeChild(menu);
				Main.mainStage.removeChild(buttonPlay);
				Main.mainStage.removeChild(buttonHowToPlay);
				Main.mainStage.removeChild(buttonCredits);
				
				menu = null;
				buttonPlay = null;
				buttonHowToPlay = null;
				buttonCredits = null;
			}
			
			if (buttonArrow != null)
			{
				//remove events
				buttonArrow.removeEventListener(MouseEvent.MOUSE_OVER, evButtonArrowOver);
				buttonArrow.removeEventListener(MouseEvent.MOUSE_OUT, evButtonArrowOut);
				buttonArrow.removeEventListener(MouseEvent.MOUSE_DOWN, evButtonArrowDown);
				buttonArrow.removeEventListener(MouseEvent.MOUSE_UP, evButtonArrowUp);
				//remove objects
				Main.mainStage.removeChild(buttonArrow);
				
				buttonArrow = null;
				
				//here selecction how screen deleted
				if (howToPlay != null)
				{
					Main.mainStage.removeChild(howToPlay);
					howToPlay = null;
				}
				if (credits != null)
				{
					Main.mainStage.removeChild(credits);
					credits = null
				}
			}
		}
	}
}