package GUI
{
	import flash.events.MouseEvent;
	import flash.sampler.startSampling;
	import flash.text.AntiAliasType;
	import flash.ui.Mouse;

	public class HUD
	{
		public var life:MClife;
		public var score:MCscore;
		public var move:MCmove;
		public var screenDamage:MCdamage;
		public var level:MClevel;
		
		public var timeToScore:int = 7000;
		public var currentTimeToScore:int = timeToScore;
		public var pointsToTime:int = 1;
		
		
		public function spawn():void{
			
			life = new MClife;
			score = new MCscore;
			move = new MCmove;
			level = new MClevel;
			screenDamage = new MCdamage;
						
			Main.layer4.addChild(level);
			Main.layer4.addChild(screenDamage);
			Main.layer4.addChild(life);
			Main.layer4.addChild(score);
			Main.layer4.addChild(move);
			
			level.alpha = screenDamage.alpha = 0;
			level.x = Main.mainStage.stageWidth/2;
			level.y = Main.mainStage.stageHeight/2;
			
			life.y = 50;
			life.x = Main.mainStage.stageWidth/2;
			life.gotoAndStop(100);
			
			move.x = Main.mainStage.stageWidth - move.width/2;
			move.y = Main.mainStage.stageHeight - move.height/2;
			
			score.x = move.x;
			score.y = Main.mainStage.stageHeight-score.height*2;
			score.mc_points.text = "00";
			
			Main.mainStage.addEventListener(MouseEvent.CLICK, evAlpha100);
			
		}
		
		protected function evAlpha100(event:MouseEvent):void{
			if (score != null)
				score.alpha = 0.8;
			
			if (move != null)
				move.alpha = 0.8;
			
			if (life != null)
				life.alpha = 0.8;
			
		}
		
		public function evUpdate():void{
			evTimeToScore();
			evAlpha0();
			evMove();
		}
		
		private function evMove():void{
			
			if (move != null){
				
				if (Main.hero != null && Main.hero.isMove){
					move.mc_move.startDrag();
					if (move.mc_move.x > 100)
						move.mc_move.x = 99;
					if (move.mc_move.x < -100)
						move.mc_move.x = -99;
					if (move.mc_move.y > 100)
						move.mc_move.y = 99;
					if (move.mc_move.y < -100)
						move.mc_move.y = -99;
				}else{
					move.mc_move.stopDrag();
					move.mc_move.x -= move.mc_move.x/10;
					move.mc_move.y -= move.mc_move.y/10;
				}
			}
		}
		
		public function evAlpha0():void
		{
			if (score != null && score.alpha > 0.1)
				score.alpha -= 0.01;
			
			if (move != null && move.alpha > 0.1)
				move.alpha -= 0.01;
			
			if (life != null && life.alpha > 0.1)
				life.alpha -= 0.01;
			
			if (screenDamage != null && screenDamage.alpha > 0)
				screenDamage.alpha -= 0.05;
			
			if (level != null && level.alpha > 0)
				level.alpha -= 0.005;
		}
		
		public function evTimeToScore():void{
			
			currentTimeToScore -= 1000/Main.mainStage.frameRate;
			
			if (currentTimeToScore <= 0){
				
				currentTimeToScore = timeToScore;
				addScore(pointsToTime);
			}
		}
		
		public function addScore(points:int):void
		{
			if (score != null){
				
				Main.totalPoints += points;
				if (Main.totalPoints < 10)
				{
					score.mc_points.text = "0"+Main.totalPoints;
				}else
				{
					score.mc_points.text = ""+Main.totalPoints;
				}
			}
		}
		
		public function evLevel(i:int):void{
			
			if (level != null){
				Main.lvl += i;
				
				if (Main.totalPoints < 10){
					level.tb_level.text = "0"+Main.lvl;
				}else level.tb_level.text = ""+Main.lvl;
			}
		}
		
		//-------------------------events life
		public function removeLife(damage:int):void{
			
			if (life != null){
				
				var x:Number = Main.instace.getRandom(0,2);
				x > 1 ? Main.audioDamage1.play(): Main.audioDamage2.play();
				
				var i:int = life.currentFrame - damage;
				if (i > 100) i = 100;
				if (i < 1) i = 1;
				if (i <2){ Main.instace.evGameOver();
				}else if (i > 2 && i < 100) life.gotoAndStop(i);
			}
		}
		
		public function addLife(addLife:int):void{
			
			if (life != null){
				var i:int = life.currentFrame + addLife;
				if (i > 100) i = 100;
				if (i > 2 && i < 100) life.gotoAndStop(i);
			}
		}
		//-------------------------end events life
		
		public function evRemove():void{
			
			Main.mainStage.removeEventListener(MouseEvent.CLICK,evAlpha100);
			currentTimeToScore = timeToScore;
				
			if (score != null){
				Main.layer4.removeChild(score);
				score = null;
				Main.layer4.removeChild(life);
				life = null;
				Main.layer4.removeChild(move);
				move = null;
				Main.layer4.removeChild(screenDamage);
				screenDamage = null;
				Main.layer4.removeChild(level);
				level = null;
			}			
		}
	}
}